package com.beone.generator;


import com.baomidou.mybatisplus.generator.config.po.TableField;

import java.util.List;

/**
 * @title  表单属性
 * @Author 覃球球
 * @Version 1.0 on 2018/11/1.
 * @Copyright 贝旺科技
 */
public class FormField {

    private String formType; //表单控件类型
    private String labelName; //显示名称
    private boolean required; //是否必填
    private boolean appear = true;  //是否在页面出现
    private String formFieldName;
    private boolean primaryKey;

    private List<FormLabelInfo> values;  // radio, checkbox, select 控件的  value, label信息

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getFormFieldName() {
        return formFieldName;
    }

    public void setFormFieldName(String formFieldName) {
        this.formFieldName = formFieldName;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isAppear() {
        return appear;
    }

    public void setAppear(boolean appear) {
        this.appear = appear;
    }

    public List<FormLabelInfo> getValues() {
        return values;
    }

    public void setValues(List<FormLabelInfo> values) {
        this.values = values;
    }
}