package com.beone.admin.controller.base;

import com.beone.admin.security.filter.ValidateCodeUsernamePasswordAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * @Title  验证码控制器
 * @Author 覃忠君 on 2017/4/20.
 * @Copyright © 蜂投网
 */
@Controller
@RequestMapping("/getCode")
public class ValidateCodeController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("")
    public String getCode(HttpServletRequest request,  HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        ServletOutputStream sos = null;
        Graphics g = null;
        try {
            response.setContentType("image/jpeg");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);

            int width = 60, height = 34;
            BufferedImage image = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
            g = image.getGraphics();
            Random random = new Random();
            g.setColor(getRandColor(200, 250));
            g.fillRect(0, 0, width, height);
            g.setFont(new Font("Times New Roman", Font.PLAIN, 25));
            g.setColor(getRandColor(160, 200));
            for (int i = 0; i < 155; i++) {
                int x = random.nextInt(width);
                int y = random.nextInt(height);
                int xl = random.nextInt(12);
                int yl = random.nextInt(12);
                g.drawLine(x, y, x + xl, y + yl);
            }

            String sRand = "";
            for (int i = 0; i < 4; i++) {
                String rand = String.valueOf(random.nextInt(10));
                sRand += rand;
                g.setColor(new Color(20 + random.nextInt(110), 20 + random
                        .nextInt(110), 20 + random.nextInt(110)));
                g.drawString(rand, 13 * i + 6, 27);
            }
            session.setAttribute(ValidateCodeUsernamePasswordAuthenticationFilter
                    .DEFAULT_SESSION_VALIDATE_CODE_FIELD, sRand);
            sos = response.getOutputStream();
            ImageIO.write(image, "JPG", sos);
        } catch (Exception e) {
            logger.info("验证码创建异常.", e);
        } finally {
            if (sos != null)
                sos.close();
            if (g != null)
                g.dispose();
        }
        return null;
    }

    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }
}
