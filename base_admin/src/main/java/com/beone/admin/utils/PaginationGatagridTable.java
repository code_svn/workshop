package com.beone.admin.utils;

import java.util.Collection;

/**
 * @title  Jquery EasyUi datagrid Table JSON 封装
 * @Author 覃球球
 * @Version 1.0 on 2018/1/26.
 * @Copyright 长笛龙吟
 */
public class PaginationGatagridTable {

    /**状态*/
    public int code=0;
    /**状态信息*/
    public String msg="";

    private Collection data;

    private long count;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Collection getData() {
        return data;
    }

    public void setData(Collection data) {
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
    /*private Collection rows;
    private Integer total; //总记录
    private Integer currPage; //当前页数
    private Integer totalPage; //总页数

    public Collection getRows() {
        return rows;
    }

    public void setRows(Collection items) {
        this.rows = items;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getCurrPage() {
        return currPage;
    }

    public void setCurrPage(Integer currPage) {
        this.currPage = currPage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }*/
}
