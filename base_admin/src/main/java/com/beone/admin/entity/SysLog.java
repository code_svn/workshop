package com.beone.admin.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title 日志管理 实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
@TableName("base_sys_log")
public class SysLog extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 存储ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 用户名
     */
	private String username;
    /**
     * IP
     */
	@TableField("IP")
	private String ip;
    /**
     * 操作类型
     */
	private String type;
    /**
     * 日志内容
     */
	private String content;
    /**
     * 请求参数
     */
	private String param;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "SysLog{" +
			", id=" + id +
			", username=" + username +
			", ip=" + ip +
			", type=" + type +
			", content=" + content +
			", param=" + param +
			", createTime=" + createTime +
			"}";
	}
}
