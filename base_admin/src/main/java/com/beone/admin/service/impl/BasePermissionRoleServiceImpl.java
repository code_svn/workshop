package com.beone.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.base.SuperServiceImpl;
import com.beone.admin.entity.BasePermissionRole;
import com.beone.admin.mapper.BasePermissionRoleMapper;
import com.beone.admin.service.BasePermissionRoleService;

import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Title 运维数据_角色对应的权限配置 服务实现类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@Service("BasePermissionRoleService")
public class BasePermissionRoleServiceImpl extends SuperServiceImpl<BasePermissionRoleMapper, BasePermissionRole> implements BasePermissionRoleService {

    /**
     * 根据角色Id, 删除角色与权限之间的关系
     * @param roleId
     */
    public void deleteAccessRoleByRoleId(Integer roleId){
        EntityWrapper<BasePermissionRole> ew = new EntityWrapper<BasePermissionRole>();
        ew.eq("role_id", roleId);
        baseMapper.delete(ew);
    }

    /**
     * 批量插入角色与权限关系
     * @param items
     */
    public void insertBatchAccessRole(List<BasePermissionRole> items){
        baseMapper.insertBatchPermissionRole(items);
    }

    /**
     * 根据角色Id 获取角色对应的权限Id
     */
    public List<BasePermissionRole> getPermissionIdByRoleId(Integer roleId){
        EntityWrapper<BasePermissionRole> ew = new EntityWrapper<BasePermissionRole>();
        ew.eq("role_id", roleId);
        return baseMapper.selectList(ew);
    }
}
