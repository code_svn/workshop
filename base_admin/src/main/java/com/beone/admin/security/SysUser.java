package com.beone.admin.security;

import com.beone.admin.entity.BaseUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @Title  系统用户
 * @Author 覃忠君 on 2017/12/20.
 * @Copyright © 长笛龙吟
 */
public class SysUser implements UserDetails{
    private  Collection<GrantedAuthority> authorities; //权限集合

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    private BaseUser user;  //后台登录用户

    public BaseUser getUser() {
        return user;
    }

    public void setUser(BaseUser user) {
        this.user = user;
    }

    /**
     * 权限集合
     * @return
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * 获取密码
     * @return
     */
    public String getPassword() {
        return user.getUserPwd();
    }

    /**
     * 用户名
     * @return
     */
    public String getUsername() {
        return user.getUserAccount();
    }

    /**
     * 账户没有过期
     * @return
     */
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账户没有被锁定
     * @return
     */
    public boolean isAccountNonLocked() {
        return "01".equals(user.getUserStatus());  // 01 正常  09 锁定
    }

    /**
     * 证书没有过期
     * @return
     */
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 账户是否有效
     * @return
     */
    public boolean isEnabled() {
        return true;
    }
}
