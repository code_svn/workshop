package com.base.common.datasource;

/**
 * @title  数据源 KEY 设定; 程序运行时将数据源切换绑定到ThreadLocal对象上
 * @Author 覃球球
 * @Version 1.0 on 2017/12/6.
 * @Copyright 长笛龙吟
 */
public class DbContextHolder {

    public enum DbType{ MASTER,SLAVE }

    private static final ThreadLocal<DbType> contextHolder = new ThreadLocal<DbType>();

    public static void setDbType(DbType dbType){
        if(dbType == null) {
            throw new NullPointerException();
        }
        contextHolder.set(dbType);
    }

    public static DbType getDbType(){
        return contextHolder.get() == null ? DbType.MASTER : contextHolder.get();
    }

    public static void clearDbType(){
        contextHolder.remove();
    }

}
